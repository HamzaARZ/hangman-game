/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penduclient;

import java.io.BufferedReader;
import java.io.PrintStream;

/**
 *
 * @author Lenovo
 */
public class Game {
    //private final static int nombreEssais = 10;
    
    private String clientName;
    private String word;
    private PrintStream out;
    private BufferedReader in;
    private int score;

    public Game(String clientName, String word, PrintStream out, BufferedReader in) {
        this.clientName = clientName;
        this.word = word;
        this.out = out;
        this.in = in;
        this.score = 10;
    }

    public String getWord() {
        return word;
    }

    public int getScore() {
        return score;
    }
    
    
    public void play(String character){
        try{
            out.println(character);
            String result = in.readLine();
            
            if(result.equals("already chosen")){
                System.out.println("this character is alreaady chosen");
                System.out.println("try again");
                
            }else if(result.equals("correct")){
                System.out.println("correct :D");
                this.word = in.readLine();
                

            }else{
                System.out.println("incorrect !!");
                score -= 1;
            }

        }catch(Exception ex){
            System.err.println(ex.getMessage());
        }
    }
   
    
}
